import express from 'express';
import mongoose from 'mongoose';
import cors from 'cors';
// intialize app
const app = express();
//routes
import userRoute from './api/routes/users.js';
import productRoute from './api/routes/products.js';


// middleware
app.use(express.json());
app.use(express.urlencoded({
  extended: true
}));
app.use(cors());

//db
mongoose.connect('mongodb://localhost:27017/arslan',
                {useCreateIndex: true},
                {useNewUrlParser: true},
                {useUnifiedTopology: true},
                {useFindAndModify: false}).then(()=>{
                  console.log("Database connected successfully...")
                }).catch(err=>console.log(err.msg));

app.use('/users',userRoute);
app.use('/products',productRoute)

const port = process.env.port || 5000;

app.listen(port, ()=> console.log(`Server is running at port ${port}`))