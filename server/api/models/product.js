import mongoose from 'mongoose';

const productSchema= new mongoose.Schema({
    productName:{
        type:String,
        required:true
    },
    productReg:{
        type:String,
        required:true
    },
    productPkg:{
        type:String,
        required:true
    },
    productPrice:{
        type:String,
        required:true
    },
    date:{
        type:Date,
        default:Date.now()
    }
})
const productModel=mongoose.model('Product',productSchema);
export default productModel;