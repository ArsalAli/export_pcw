import mongoose from 'mongoose';

const userSchema= new mongoose.Schema({
    custId:{
        type:String,
        required:true
    },
    companyName:{
        type:String,
        required:true
    },
    companyAddress:{
        type:String,
        required:true
    },
    country:{
        type:String,
        required:true
    },
    customerName:{
        type:String,
        required:true
    },
    email:{
        type:String,
        required:true,
        unique:true
    },
    password:{
        type:String,
        required:true
    },
    companyPhone:{
        type:String,
        required:true
    },
    isAdmin:{
        type:Boolean,
        default:false
    },
    isCustomer:{
        type:Boolean,
        default:false
    },
    date:{
        type:Date,
        default:Date.now()
    }
})
const userModel=mongoose.model('User',userSchema);
export default userModel;