import Product from '../models/product.js';

// test
export const userTest =(req,res)=>{
    res.send('User test route working.....')
}
// all product
export const productData = (req,res)=>{
    Product.find({})
        .then(products=>{
            if(products){
                return res.status(200).json({msg:"Success", products});
            }else{
                return res.status(400).json({msg:"No User Found!..."});
            }
        })
}   
//router: products/delete/:Id
export const productDelete = (req,res)=>{
    const delete_id = req.params.id;
    if(delete_id){
        Product.findByIdAndDelete(delete_id)
            .then(product=>{
                if(product){
                   return res.status(200).json({msg:`${product.productName} delete Successfully.`});
                }
            }).catch(err=>{
                res.status(500).json({ msg:"Server Error!...."});
            })
    }
}
//route : users/register
export const newProduct = async( req, res ) =>{
    
    const { productName,
            productReg,
            productPkg,
            productPrice } = req.body.product;
    // check validate
    if( productName === '' || productName === undefined){
       return res.status(400).json({ msg:"Enter the Product Name..."})
    }

    if( productReg === '' || productReg === undefined){
        return res.status(400).json({ msg:"Enter the Product Registration Number..."})
     }
     if( productPkg === '' || productPkg === undefined){
        return res.status(400).json({ msg:"Enter the Product Packaging..."})
     }
     if( productPrice === '' || productPrice === undefined){
        return res.status(400).json({ msg:"Enter the Product Price..."})
     }
  
    //normal user
    const isProduct = await Product.findOne({ productReg });
    if( isProduct ){
        return res.status(400).json({msg:"Product already exist..."});
    }else{
        const newProduct = new Product({
            productName,
            productReg,
            productPkg,
            productPrice
        });
        if(newProduct){
            newProduct.save()
                        .then(newProduct=>{
                            res.status(200).json({msg:"New Product Added Successfully",newProduct});
                        }).catch(err=>{
                            res.status(500).json({ msg:"Server Error!...."});
                        })
        }
    }
    
}
