import User from '../models/users.js';
import jwt from 'jsonwebtoken';



// test
export const userTest =(req,res)=>{
    res.send('User test route working.....')
}
// all users
export const userData = (req,res)=>{
    User.find({})
        .then(users=>{
            if(users){
                return res.status(200).json({msg:"Success", users});
            }else{
                return res.status(400).json({msg:"No User Found!..."});
            }
        })
}   
//router: users/delete/:Id
export const userDelete = (req,res)=>{
    const delete_id = req.params.id;
    if(delete_id){
        User.findByIdAndDelete(delete_id)
            .then(user=>{
                if(user){
                   return res.status(200).json({msg:`${user.customerName} User delete Successfully.`});
                }
            }).catch(err=>{
                res.status(500).json({ msg:"Server Error!...."});
            })
    }
}
//route : users/register
export const userRegister = async( req, res ) =>{
    
    const { custId,
            companyName,
            companyAddress,
            companyPhone,
            country,
            customerName,
            email,
            password } = req.body.customer;
    // check validate
    if( email === '' || email === undefined){
       return res.status(400).json({ msg:"Enter the Email..."})
    }

    if( password === '' || password === undefined){
        return res.status(400).json({ msg:"Enter the Password..."})
     }
     if( custId === '' || custId === undefined){
        return res.status(400).json({ msg:"Enter the Unique Id..."})
     }
     if( companyName === '' || companyName === undefined){
        return res.status(400).json({ msg:"Enter the Company Name..."})
     }
     if( customerName === '' || customerName === undefined){
        return res.status(400).json({ msg:"Enter the Customer Name..."})
     }
     if( companyPhone === '' || companyPhone === undefined){
        return res.status(400).json({ msg:"Enter the Company Phone..."})
     }
  
    //normal user
    const isUser = await User.findOne({ email });
    if( isUser ){
        return res.status(400).json({msg:"User already exist..."});
    }else{
        const newCustomer = new User({
            custId,
            companyName,
            companyAddress,
            companyPhone,
            country,
            customerName,
            email,
            password
        });
        if(newCustomer){
            newCustomer.save()
                        .then(newUser=>{
                            res.status(200).json({msg:"New User Added Successfully",newUser});
                        }).catch(err=>{
                            res.status(500).json({ msg:"Server Error!...."});
                        })
        }
    }
    
}
//route : users/login
export const userLogin = async( req, res ) =>{
    
    const { email, password } = req.body;
    // check validate
    if( email === '' || email === undefined){
       return res.status(400).json({ msg:"Enter the Email..."})
    }

    if( password === '' || password === undefined){
        return res.status(400).json({ msg:"Enter the Password..."})
     }
    // admin direct login
    if( email === 'pcwexp@gmail.com' && password === 'exp_321'){
        let payload ={
            email,
            password,
            isAdmin:true
        }
        jwt.sign( payload,'export_Secret' ,(err,token)=>{
            if (err) throw err;
            res.status(200).json({
                msg:"Success",
                token,
                payload
            })
        })
    }else{
        //normal user
        const isUser = await User.findOne({ email });
        if( !isUser ){
            return res.status(400).json({msg:"User not exist..."});
        }else{
            if( isUser.password !== password ){
                return res.status(400).json({msg:"Enter correct password..."});
            }
            let payload ={
                email,
                password,
                isCustomer:true
            }
            jwt.sign( payload,'export_Secret' ,( err,token )=>{
                if(err) throw err;
                res.status(200).json({
                    msg:"Success",
                    token,
                    payload
                })
            })
        }
    }
}