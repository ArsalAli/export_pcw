import express from 'express';
const router = express.Router();
//controllers 
import { newProduct,productDelete,productData } from '../controllers/products.js';

//register
router.post('/new-product', newProduct);
//get all product
router.get('/get-product', productData);
//delete product by id 
router.delete('/delete/:id',productDelete);

export default router;