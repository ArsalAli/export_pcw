import express from 'express';
const router = express.Router();
//controllers 
import {userTest, userLogin, userRegister,userData,userDelete } from '../controllers/users.js';

//test
router.get('/',userTest);
//login
router.post('/login', userLogin );
//register
router.post('/register', userRegister)
//get all user
router.post('/get-users', userData);
//delete user by id 
router.delete('/delete/:id',userDelete);

export default router;